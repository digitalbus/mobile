import React from 'react'
import {StyleSheet, Text, View} from "react-native";
import {routeMapping} from "../utils/MappingUtils";
import RightArrow from "../assets/img/BlackRightArrow.svg";


class FromAndDestination extends React.Component {
    render() {
        let ultimateArrowStyle =  () => {
            if(this.props.white){
                return [this.props.arrow,{fill:'white'}]
            }else{
                return this.props.arrow
            }
        }

        return (
            <View style={{flexDirection: 'row'}}>

                <Text style={this.props.innerStyleFrom}>{routeMapping(this.props.queue.timetable.route.departure)}</Text>

                <RightArrow width={50} height={25} style={ultimateArrowStyle()}/>

                <Text style={this.props.innerStyleDestination}>{routeMapping(this.props.queue.timetable.route.arrival)}</Text>
            </View>
        );
    }
}



export default FromAndDestination