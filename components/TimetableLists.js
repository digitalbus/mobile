import React, { Component } from 'react';
import { Image,View } from 'react-native';
import { Container, Header, Content, Card,Text, CardItem, Thumbnail, Button, Title, Left, Body, Right } from 'native-base';
import Route from '../screen/Route';
import Dialog, { DialogContent,DialogFooter,DialogButton } from 'react-native-popup-dialog';
import TimetableCard from './TimetableCard'
import env from 'react-native-config'

export default class Timetable extends Component {

    constructor(props) {
        super(props)
        this.state = {
            visible:false,
            selectedTimetable:null
        }
    }

    handleButtonPress = (timetable) => {
        this.setState({selectedTimetable:timetable,visible:true})
    }

    handleRequest = (queue) => {
        this.props.navigation.navigate('Reserve', { "queue": queue })
    }

    handleReserveQueue = async () => {
        let {selectedTimetable} = this.state
        let data = {}
        if(selectedTimetable){
            try {
                data = await fetch(`${env.queue_service}/queue`,
                    {
                        method: 'POST',
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            'userId': "1",
                            'timetableId': selectedTimetable.id,
                            'queueType': "reserve"
                        }),
                    })
                data = await data.json()
                this.handleRequest(data)
            } catch (error) {
                console.error(error)
            }
        }
        this.setState({visible:false})
    }

    dialog = () => {
        if(this.state.selectedTimetable){
            return(
                <View>
                    <Dialog
                        visible={this.state.visible}
                        footer={
                            <DialogFooter>
                                <DialogButton
                                    text="CANCEL"
                                    onPress={() => {this.setState({visible:false})}}
                                />
                                <DialogButton
                                    text="OK"
                                    onPress={() => this.handleReserveQueue()}
                                />
                            </DialogFooter>
                        }
                    >
                        <DialogContent>
                            <Text>Are you Sure you want to reserve</Text>
                        </DialogContent>
                    </Dialog>
                </View>
            )
        }else{
            return (<View></View>)
        }
    }


    render() {
        const { timetables } = this.props
        return (
            <Container>
                <Content>
                    <Title><Text>Timetable</Text></Title>
                    {timetables.map(timetable => (
                       <TimetableCard timetable={timetable} handleButtonPress={this.handleButtonPress}/>
                    ))}
                </Content>
                {this.dialog()}
            </Container>
        );
    }
}