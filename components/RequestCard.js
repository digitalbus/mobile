import React from 'react'
import {Dimensions, StyleSheet, Text, TouchableOpacity, View} from "react-native"
import moment from "moment"
import LinearGradient from "react-native-linear-gradient"

const {width: dWidth, height: dHeight} = Dimensions.get('window')

class RequestCard extends React.Component {


    render() {
        const {timetable} = this.props
        const timeFormat = time => moment(time, 'hh:mm:ss').format('h:mm a')
        return (
            <TouchableOpacity  style={styles.TimetableContainer}>
                <View style={styles.Timetable}>
                    <View>
                        <LinearGradient style={styles.reservedTag} colors={['#fda90a', '#fe4f78']}>
                            <Text style={styles.reservedText}>Requested</Text>
                        </LinearGradient>
                        <View style={{marginLeft: 20,marginRight: 10}}>
                            <View style={styles.TimetableText}>
                                <Text style={styles.time}>{timeFormat(timetable.time)}</Text>
                                <Text style={styles.seatsLeft}>{timetable.availableSeat}</Text>
                            </View>
                            <View style={styles.TimetableText}>
                                <Text style={styles.busTypeText}>Type : {timetable.busType}</Text>
                                <Text style={[styles.seatsLeftText]}>Seats Left</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}

let styles = StyleSheet.create({
    TimetableContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        width: '100%',
        height: 100,
        top: -48,
    },
    Timetable: {
        flex: 1,
        justifyContent: 'center',
        // borderRadius: 15,
        borderRadius: 15,
        backgroundColor: 'white',
        width: 350,
        height: 88,
        position: 'absolute',
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 10,
    },
    TimetableText: {
        fontSize: 12,
        flexDirection: 'row',
        justifyContent: 'space-between',
        fontFamily: 'Montserrat-ExtraBold',
        color: '#2A2D33',
        marginLeft: 24
    },
    time: {
        fontFamily: 'Montserrat-ExtraBold',
        fontSize: 24,
        color: '#2A2D33',
        marginTop: 20
    },
    seatsLeft: {
        fontSize: 24,
        fontFamily: 'Montserrat-ExtraBold',
        color: '#FE4F78',
        marginRight: 45,
        marginTop: 20
    },
    seatsLeftText: {
        marginBottom: 20,
        fontFamily: 'Montserrat-SemiBold',
        color: '#2A2D33'
    },
    busTypeText: {
        fontFamily: 'Montserrat-SemiBold',
        marginBottom: 20,
        color: '#2A2D33'
    },
    reservedText: {
        transform: [{"rotate": '270deg'}],
        // bottom: 10,
        // right: 10,
        // width: '100%',
        // height: '100%',
        width: 88,
        right: 28,
        top: 20,
        fontSize: 10,
        fontFamily: 'Montserrat-ExtraBold',
        color: 'black'
    },
    reservedTag: {
        top:2,
        position: 'absolute',
        borderBottomLeftRadius: 15,
        borderTopLeftRadius: 15,
        height: 88,
        width: 30
    }
})

export default RequestCard
