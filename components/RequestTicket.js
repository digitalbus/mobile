import React from 'react'
import {StyleSheet, Text, View} from "react-native"
import {Item} from "native-base"
import LinearGradient from "react-native-linear-gradient"
import QRCode from "react-native-qrcode-svg"
import env from "react-native-config"
import moment from "moment"
import FromAndDestination from "../components/FromAndDestination"
import {Col, Grid} from "react-native-easy-grid"


const timeFormat = time => moment(time, 'hh:mm:ss').format('h:mm a')

class RequestedScreen extends React.Component {

    render() {
        return (
            <View style={styles.ticketContainer}>
                <View style={styles.firstPartOfTicket}>
                    <Item style={styles.ticketHeader}>
                        <View style={{height: 40, justifyContent: 'center', marginTop: 16}}>
                            <FromAndDestination queue={this.props.queue} innerStyleFrom={styles.routeFrom}
                                                innerStyleDestination={styles.routeDestination}
                                                arrow={styles.rightArrowStyle}/>
                            {/*<Text style={styles.dateText}>Date Thursday, August 22nd 2019</Text>*/}
                        </View>
                    </Item>

                    <View>
                        <Text style={styles.upperContent}>Requested Round</Text>
                    </View>

                    <Grid style={styles.ticketContent}>
                        <Col style={{height: 40, marginLeft: 20}}>
                            <View>
                                <Text style={styles.mTitle}>Time</Text>
                                <Text
                                    style={styles.blackText}>{moment(this.props.queue.reservedQueue.timetable.time, 'h:mm:ss').format('h:mm a')}</Text>
                            </View>

                            <View style={styles.upperContent} style={{marginTop: 16}}>
                                <Text style={styles.mTitle}>Passenger</Text>
                                {/*<Text>Patric Emmel</Text>*/}
                                <Text
                                    style={styles.blackText}>{`${this.props.queue.reservedQueue.passenger.firstName} ${this.props.queue.reservedQueue.passenger.lastName.substring(0, 1)}.`}</Text>
                            </View>
                        </Col>
                        <Col style={{height: 40, marginRight: -20}}>
                            <View>
                                <Text style={styles.mTitle}>Bus Type</Text>
                                {/*<Text>Van</Text>*/}
                                <Text style={styles.blackText}>{this.props.queue.reservedQueue.timetable.busType}</Text>
                            </View>

                            <View style={{marginTop: 16}}>
                                <Text style={styles.mTitle}>Queue No.</Text>
                                <Text style={styles.blackText}>{'not approve'}</Text>
                                {/*<Text style={styles.blackText}>{this.props.queueNumber}</Text>*/}
                            </View>
                        </Col>
                    </Grid>
                </View>

                <View style={styles.secondPartOfTicket}>
                    <Item></Item>

                    <View>
                        <Text style={styles.upperContent}>Reserved Round</Text>
                    </View>

                    <Grid style={styles.ticketContent}>
                        <Col style={{height: 40, marginLeft: 20}}>
                            <View>
                                <Text style={styles.mTitle}>Time</Text>
                                <Text
                                    style={styles.blackText}>{moment(this.props.queue.timetable.time, 'h:mm:ss').format('h:mm a')}</Text>
                            </View>

                            <View style={styles.upperContent} style={{marginTop: 16}}>
                                <Text style={styles.mTitle}>Passenger</Text>
                                {/*<Text>Patric Emmel</Text>*/}
                                <Text
                                    style={styles.blackText}>{`${this.props.queue.passenger.firstName} ${this.props.queue.passenger.lastName.substring(0, 1)}.`}</Text>
                            </View>
                        </Col>
                        <Col style={{height: 40, marginRight: -20}}>
                            <View>
                                <Text style={styles.mTitle}>Bus Type</Text>
                                {/*<Text>Van</Text>*/}
                                <Text style={styles.blackText}>{this.props.queue.timetable.busType}</Text>
                            </View>

                            <View style={{marginTop: 16}}>
                                <Text style={styles.mTitle}>Queue No.</Text>
                                <Text style={styles.blackText}>{this.props.queueNumber}</Text>
                            </View>
                        </Col>
                    </Grid>
                </View>

                <View style={styles.separator}><Text style={styles.leftRip}/><Text style={styles.rightRip}/></View>

                <View style={styles.qrSection}><QRCode size={125} value={`${this.props.queue.id}`}/>
                    {/*<Text style={styles.idQueue}>{this.props.queue.id}</Text>*/}
                </View>

                <LinearGradient start={{x: 0, y: 1}}
                                end={{x: 1, y: 1}}
                                style={styles.cancel}
                                colors={['#fda90a', '#fe4f78']}>
                    <Text style={styles.cancelText}>CANCEL</Text>
                </LinearGradient>
            </View>
        )
    }
}

let styles = StyleSheet.create({
    ticketContainer: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    firstPartOfTicket: {
        backgroundColor: 'white',
        width: 320,
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 0,
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15,
        height: 265,
        top: -111,
        padding: 5,
    },
    secondPartOfTicket: {
        backgroundColor: 'white',
        width: 320,
        height: 195,
        top: -111,
        padding: 5,
    },
    mTitle: {
        color: '#2a2d33',
        fontSize: 13,
        fontFamily: 'Montserrat-Bold',
    },
    ticketHeader: {
        justifyContent: 'center',
        alignItems: 'flex-start',
        flexDirection: 'row',
        height: 80
    },
    qrCodeContainer: {
        alignItems: 'center',
        marginTop: 17
    },
    ticketContent: {
        marginTop: 16,
        marginLeft: 25,
        justifyContent: 'space-around',
        flexDirection: 'row',
        fontSize: 16,
        fontFamily: 'Montserrat-Black',
    },
    routesText: {
        fontSize: 18,
        color: '#2a2d33',
        fontFamily: 'Montserrat-ExtraBold',
    },
    dateText: {
        paddingTop: 9,
        alignItems: 'center'
    },
    upperContent: {
        marginTop: 25,
        marginLeft: 45,
        fontSize: 15,
        color: '#2a2d33',
        fontFamily: 'Montserrat-Bold',
        justifyContent: 'space-around',
        flexDirection: 'row',
    },
    HeaderContainer: {
        // display: 'flex',
        // justifyContent: 'center',
        // alignItems: 'center',
        textAlign: 'center',
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        flex: 1,
        flexDirection: 'row',
    },
    Header: {
        width: '100%',
        height: 250,
        flex: 1,
        // position: 'absolute',
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 10,
    },
    HeaderTextSmallFont: {
        fontSize: 14,
        fontFamily: 'Montserrat-SemiBold',
        color: 'white',
    },
    HeaderText: {
        marginTop: 16,
        fontSize: 24,
        fontFamily: 'Montserrat-ExtraBold',
        color: 'white',
        textAlign: 'center',
    },
    titleContainer: {
        width: '100%',
        height: 100,
    },
    HeaderMargin: {
        marginLeft: 16,
        marginTop: 32,
    },
    HeaderMarginText: {
        marginLeft: 16,
        marginTop: 0,
        position: 'absolute',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    TimetableContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        width: '100%',
        height: 100,
    },
    Timetable: {
        flex: 1,
        justifyContent: 'space-between',
        borderRadius: 15,
        backgroundColor: 'white',
        width: 300,
        height: 88,
        position: 'absolute',
        top: -48,
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 10,
    },
    TimetableText: {
        fontSize: 12,
        flexDirection: 'row',
        justifyContent: 'space-between',
        fontFamily: 'Montserrat-ExtraBold',
        color: '#2A2D33',
        marginLeft: 24,
        marginBottom: 6
    },
    icon: {
        color: 'white'
    },
    time: {
        fontSize: 24,
        color: '#2A2D33',
        marginLeft: 24,
        marginTop: 16
    },
    seatsLeft: {
        fontSize: 24,
        color: '#FE4F78',
        marginRight: 36,
        marginTop: 16
    },
    seatsLeftText: {
        marginRight: 20,
    },
    separator: {
        backgroundColor: 'white',
        borderStyle: 'dashed',
        width: 280,
        borderColor: "#ffffff",
        borderWidth: 1,
        top: -111,
        zIndex: 1,
        borderRadius: 1,
    },
    leftRip: {
        width: 25,
        height: 25,
        bottom: -14,
        left: -32,
        position: 'absolute',
        backgroundColor: '#F6F7FF',
        borderRadius: 25
    },
    rightRip: {
        width: 25,
        height: 25,
        bottom: -14,
        right: -32,
        position: 'absolute',
        backgroundColor: '#F6F7FF',
        borderRadius: 25
    },
    qrSection: {
        width: 320,
        height: 180,
        backgroundColor: 'white',
        top: -111,
        marginBottom: 50,
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        borderRadius: 15
    },
    idQueue: {
        marginTop: 10,
        fontFamily: 'Montserrat-SemiBold',
        fontSize: 16
    },
    blackText: {
        fontFamily: 'Montserrat-Medium',
        color: '#2A2D33'
    },
    HeaderMarginCenter: {
        marginTop: 80,
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'row',
        width: '100%',
    },
    routeFrom: {
        fontSize: 16,
        color: '#2a2d33',
        fontFamily: 'Montserrat-ExtraBold',
    },
    routeDestination: {
        fontSize: 16,
        color: '#2a2d33',
        fontFamily: 'Montserrat-ExtraBold',
    },
    rightArrowStyle: {
        marginTop: 2
    },
    cancel: {
        flex: 1,
        borderRadius: 20,
        width: 320,
        height: 48,
        position: 'absolute',
        top: 560,
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    cancelText: {
        fontSize: 14,
        fontFamily: 'Montserrat-ExtraBold',
        color: 'white'
    }
})

const ripImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAYAAAACCAYAAAB7Xa1eAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuOWwzfk4AAAAaSURBVBhXY5g7f97/2XPn/AcCBmSMQ+I/AwB2eyNBlrqzUQAAAABJRU5ErkJggg=="


export default RequestedScreen