import React from 'react'
import { Image,View } from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body } from 'native-base';
import QRCode from 'react-native-qrcode'


export default ({ route, queueId, children }) => (
    <Card style={{ flex: 0, textAlign: 'center' }}>
        <CardItem>
            <Left>
                <Body>
                    <Text>Reserved!</Text>
                </Body>
            </Left>
        </CardItem>
        <CardItem>
            <Body>
                <QRCode value={queueId+''} />
                <Text >date</Text>
                <Text >{route.day}</Text>
                <Text>Time</Text>
                <Text note>{route.time}</Text>
            </Body>
        </CardItem>
        <CardItem>
            <View>
                {children}
            </View>
            <Left>
                <Button transparent textStyle={{ color: '#87838B' }}>
                    <Icon name="alarm" />
                </Button>
            </Left>
        </CardItem>
    </Card>
)