import React from "react"
import {StyleSheet} from "react-native"
import {Container, Content, List, ListItem, Text} from "native-base"
import PushNotification from 'react-native-push-notification'

const routes = ["Route", "MyTickets", "Test", "RequestedScreen"]
export default class SideBar extends React.Component {
    render() {
        return (
            <Container style={{borderTopRightRadius: 5, borderBottomRightRadius: 5}}>
                <Content>
                    <Text style={styles.appTitle}>BOURNEY</Text>
                    <List
                        dataArray={routes}
                        renderRow={data => {
                            return (
                                <ListItem
                                    button
                                    onPress={() => this.props.navigation.navigate(data)}>
                                    <Text>{data}</Text>
                                </ListItem>
                            )
                        }}
                    />
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    appTitle: {
        margin: 5,
        fontSize: 24,
        fontFamily: 'Montserrat-ExtraBold',
        color: 'black',
    }
})