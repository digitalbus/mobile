import React from 'react'
import {StyleSheet, Text, TouchableOpacity, View} from "react-native"
import moment from "moment"

class TimetableCard extends React.Component {


    render() {
        const {timetable} = this.props
        const timeFormat = time => moment(time, 'hh:mm:ss').format('h:mm a')
        return (
            <TouchableOpacity onPress={this.props.selectTimetable} style={styles.TimetableContainer}>
                <View style={styles.Timetable}>
                    <View style={styles.TimetableText}>
                        <Text style={styles.time}>{timeFormat(timetable.time)}</Text>
                        <Text style={styles.seatsLeft}>{timetable.availableSeat}</Text>
                    </View>
                    <View style={styles.TimetableText}>
                        <Text style={styles.busTypeText}>Type : {timetable.busType}</Text>
                        <Text style={[styles.seatsLeftText]}>Seats Left</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}

let styles = StyleSheet.create({
    TimetableContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        width: '100%',
        height: 100,
    },
    Timetable: {
        flex: 1,
        justifyContent: 'space-between',
        borderRadius: 15,
        backgroundColor: 'white',
        width: 350,
        height: 88,
        position: 'absolute',
        top: -48,
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 10,
    },
    TimetableText: {
        fontSize: 12,
        flexDirection: 'row',
        justifyContent: 'space-between',
        fontFamily: 'Montserrat-ExtraBold',
        color: '#2A2D33',
        marginLeft: 24
    },
    time: {
        fontFamily: 'Montserrat-ExtraBold',
        fontSize: 24,
        color: '#2A2D33',
        marginTop: 20
    },
    seatsLeft: {
        fontSize: 24,
        fontFamily: 'Montserrat-ExtraBold',
        color: '#FE4F78',
        marginRight: 45,
        marginTop: 20
    },
    seatsLeftText: {
        marginRight: 24,
        marginBottom: 20,
        fontFamily: 'Montserrat-SemiBold',
        color: '#2A2D33'
    },
    busTypeText: {
        fontFamily: 'Montserrat-SemiBold',
        marginBottom: 20,
        color: '#2A2D33'
    },
})

export default TimetableCard
