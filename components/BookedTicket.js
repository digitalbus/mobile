import React from 'react'
import {Item, View} from 'native-base'
import FromAndDestination from "./FromAndDestination"
import {Col, Grid} from "react-native-easy-grid"
import {StyleSheet, Text} from "react-native"
import moment from "moment"
import QRCode from "react-native-qrcode-svg"

class BookedTicket extends React.Component {
    render() {
        return (
            <View style={styles.ticketContainer}>
                <View style={styles.ticket}>
                    <Item style={styles.ticketHeader}>
                        <View style={{height: 40, justifyContent: 'center', marginTop: 16}}>
                            <FromAndDestination queue={this.props.queue} innerStyleFrom={styles.routeFrom} innerStyleDestination={styles.routeDestination} arrow={styles.rightArrowStyle}/>
                            {/*<Text style={styles.dateText}>Date Thursday, August 22nd 2019</Text>*/}
                        </View>
                    </Item>

                    <Grid style={styles.ticketContent}>
                        <Col style={{height: 40, marginLeft: 20}}>
                            <View style={styles.upperContent}>
                                <Text style={styles.mTitle}>Time</Text>
                                <Text
                                    style={styles.blackText}>{moment(this.props.queue.timetable.time, 'h:mm:ss').format('h:mm a')}</Text>
                            </View>

                            <View style={styles.upperContent} style={{marginTop: 16}}>
                                <Text style={styles.mTitle}>Passenger</Text>
                                {/*<Text>Patric Emmel</Text>*/}
                                <Text
                                    style={styles.blackText}>{`${this.props.queue.passenger.firstName} ${this.props.queue.passenger.lastName.substring(0, 1)}.`}</Text>
                            </View>
                        </Col>
                        <Col style={{height: 40, marginRight: -20}}>
                            <View style={styles.upperContent}>
                                <Text style={styles.mTitle}>Bus Type</Text>
                                {/*<Text>Van</Text>*/}
                                <Text style={styles.blackText}>{this.props.queue.timetable.busType}</Text>
                            </View>

                            <View style={styles.upperContent} style={{marginTop: 16}}>
                                <Text style={styles.mTitle}>Queue No.</Text>
                                <Text style={styles.blackText}>{this.props.queueNumber}</Text>
                            </View>
                        </Col>
                    </Grid>
                </View>

                <View style={styles.separator}><Text style={styles.leftRip}/><Text
                    style={styles.rightRip}/></View>

                <View style={styles.qrSection}><QRCode size={125} value={`${this.props.queue.id}`}/>
                    {/*<Text style={styles.idQueue}>{this.state.queue.id}</Text>*/}
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    ticketContainer: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    ticket: {
        backgroundColor: 'white',
        width: 320,
        borderRadius: 15,
        height: 230,
        top: -111,
        padding: 5,
    },
    mTitle: {
        color: '#2a2d33',
        fontSize: 13,
        fontFamily: 'Montserrat-Bold',
    },
    ticketHeader: {
        justifyContent: 'center',
        alignItems: 'flex-start',
        flexDirection: 'row',
        height: 80
    },
    ticketContent: {
        marginTop: 25,
        marginLeft: 25,
        justifyContent: 'space-around',
        flexDirection: 'row',
        fontSize: 16,
        fontFamily: 'Montserrat-Black',
    },
    dateText: {
        paddingTop: 9,
        alignItems: 'center'
    },
    Header: {
        width: '100%',
        height: 250,
        flex: 1,
        // position: 'absolute',
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 10,
    },
    HeaderMargin: {
        marginLeft: 16,
        marginTop: 32,
    },
    Timetable: {
        flex: 1,
        justifyContent: 'space-between',
        borderRadius: 15,
        backgroundColor: 'white',
        width: 300,
        height: 88,
        position: 'absolute',
        top: -48,
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 10,
    },
    time: {
        fontSize: 24,
        color: '#2A2D33',
        marginLeft: 24,
        marginTop: 16
    },
    seatsLeft: {
        fontSize: 24,
        color: '#FE4F78',
        marginRight: 36,
        marginTop: 16
    },
    separator: {
        backgroundColor: 'white',
        borderStyle: 'dashed',
        width: 280,
        borderColor: "#ffffff",
        borderWidth: 1,
        top: -111,
        zIndex: 1,
        borderRadius: 1,
    },
    leftRip: {
        width: 25,
        height: 25,
        bottom: -14,
        left: -32,
        position: 'absolute',
        backgroundColor: '#F6F7FF',
        borderRadius: 25
    },
    rightRip: {
        width: 25,
        height: 25,
        bottom: -14,
        right: -32,
        position: 'absolute',
        backgroundColor: '#F6F7FF',
        borderRadius: 25
    },
    qrSection: {
        width: 320,
        height: 180,
        backgroundColor: 'white',
        top: -111,
        marginBottom: 50,
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        borderRadius: 5
    },
    idQueue: {
        marginTop: 10,
        fontFamily: 'Montserrat-SemiBold',
        fontSize: 16
    },
    blackText: {
        fontFamily: 'Montserrat-Medium',
        color: '#2A2D33'
    },
    routeFrom: {
        fontSize: 16,
        color: '#2a2d33',
        fontFamily: 'Montserrat-ExtraBold',
    },
    routeDestination:{
        fontSize: 16,
        color: '#2a2d33',
        fontFamily: 'Montserrat-ExtraBold',
    },
    rightArrowStyle: {
        marginTop: 2
    },
    upperContent: {}
})

export default BookedTicket
