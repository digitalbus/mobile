import React from 'react'
import {StyleSheet, Text, View} from "react-native"
import {Container, Content} from "native-base"
import LinearGradient from "react-native-linear-gradient"
import {FontAwesomeIcon} from "@fortawesome/react-native-fontawesome"
import {faAngleLeft} from "@fortawesome/free-solid-svg-icons"
import env from "react-native-config"
import moment from "moment"
import RequestTicket from "../components/RequestTicket"
import BookedTicket from "../components/BookedTicket"


const timeFormat = time => moment(time, 'hh:mm:ss').format('h:mm a')

class TicketScreen extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            queue: {
                timetable: {
                    route: {},
                },
                passenger: {
                    lastName: ''
                }
            }
        }
    }

    componentDidMount = async () => {
        let queue = this.props.navigation.getParam('queue')
        let queueData = await fetch(`${env.system_service}/queue/${queue.id}`)
        queueData = await queueData.json()
        let queueNumber = await fetch(`${env.system_service}/queue/queueNumber/${queue.id}`)
        queueNumber = await queueNumber.json()
        this.setState({queue: queueData, queueNumber: queueNumber})
        console.log('queueueueueueueueue', queueData)
    }

    goBack = () => {
        this.props.navigation.goBack()
    }

    render() {
        return (
            <Container style={{backgroundColor: '#F6F7FF'}}>
                <Content>
                    <LinearGradient style={styles.HeaderContainer} colors={['#fda90a', '#fe4f78']}>
                        <View style={styles.Header}>
                            <View style={styles.HeaderMargin}>
                                <FontAwesomeIcon onPress=
                                                     {this.goBack} icon={faAngleLeft}
                                                 style={styles.icon} size={24}/>
                                <Text style={styles.HeaderText}>Booking Details</Text>
                            </View>
                        </View>
                    </LinearGradient>
                    {this.state.queue.reservedQueue ?
                        <RequestTicket queueNumber={this.state.queueNumber} queue={this.state.queue}/> :
                        <BookedTicket queueNumber={this.state.queueNumber} queue={this.state.queue}/>
                    }
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    ticket: {
        backgroundColor: 'white',
        width: 320,
        borderRadius: 15,
        height: 230,
        top: -111,
        padding: 5,
    },
    HeaderContainer: {
        // display: 'flex',
        // justifyContent: 'center',
        // alignItems: 'center',
        textAlign: 'center',
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        flex: 1,
        flexDirection: 'row',
    },
    Header: {
        width: '100%',
        height: 250,
        flex: 1,
        // position: 'absolute',
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 10,
    },
    HeaderText: {
        marginTop: 16,
        fontSize: 24,
        fontFamily: 'Montserrat-ExtraBold',
        color: 'white',
        textAlign: 'center',
    },
    HeaderMargin: {
        marginLeft: 16,
        marginTop: 32,
    },
    Timetable: {
        flex: 1,
        justifyContent: 'space-between',
        borderRadius: 15,
        backgroundColor: 'white',
        width: 300,
        height: 88,
        position: 'absolute',
        top: -48,
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 10,
    },
    icon: {
        color: 'white'
    },
    time: {
        fontSize: 24,
        color: '#2A2D33',
        marginLeft: 24,
        marginTop: 16
    },
    seatsLeft: {
        fontSize: 24,
        color: '#FE4F78',
        marginRight: 36,
        marginTop: 16
    }
})

const ripImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAYAAAACCAYAAAB7Xa1eAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuOWwzfk4AAAAaSURBVBhXY5g7f97/2XPn/AcCBmSMQ+I/AwB2eyNBlrqzUQAAAABJRU5ErkJggg=="

export default TicketScreen
