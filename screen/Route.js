import React from 'react'
import {AsyncStorage, Dimensions, StyleSheet, Text, TouchableOpacity, View, Animated} from 'react-native'
import {Container, Content, Drawer, Icon, Picker} from 'native-base'
import Hamburger from '../assets/img/hamburgerbar.svg'
import LinearGradient from 'react-native-linear-gradient'
import env from 'react-native-config'
import Sidebar from "../components/Sidebar"
import {routeMapping} from "../utils/MappingUtils"
import moment from "moment"
import Geolocation from '@react-native-community/geolocation';


const {width, height} = Dimensions.get('window')
const departureList = [
    {
        name: 'Select Route',
        key: 'A'
    },
    {
        name: 'Bangmod Campus',
        key: 'BMD'
    }, {
        name: 'Bangkhuntien',
        key: 'BKT'
    }, {
        name: 'KX Building',
        key: 'KNX'
    }, {
        name: 'Lotus Rama 2',
        key: 'TLT'
    },
]

const dayList = [
    {
        name: 'Sunday',
        key: 'sunday',
    }, {
        name: 'Monday',
        key: 'monday',
    }, {
        name: 'Tuesday',
        key: 'tuesday',
    }, {
        name: 'Wednesday',
        key: 'wednesday',
    }, {
        name: 'Thursday',
        key: 'thursday',
    }, {
        name: 'Friday',
        key: 'friday',
    }, {
        name: 'Saturday',
        key: 'saturday',
    },
]

class Route extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            route: departureList,
            departurePoint: 'A',
            arrivalList: [],
            arrivalPointId: undefined,
            departureDate: dayList[moment().day()].key,
            user: {
                firstName: undefined
            }
        }
    }

    handleFindTimetable = () => {
        console.log('function', this.props.navigation.navigate('Timetable', {
                "routeId": this.state.arrivalPointId,
                'departureDate': this.state.departureDate
            })
        )
    }

    async componentDidMount() {
        this.closeDrawer()
        let user = await JSON.parse(await AsyncStorage.getItem('passenger'))
        this.setState({user: user})
    }

    changeDepartureDate = (event) => {
        this.setState({departureDate: event})
    }

    handleDepartureChange = async (event) => {
        await this.setState({departurePoint: event})
        let data = await fetch(`${env.system_service}/system/route/departure?departureAt=${this.state.departurePoint}`)
        data = await data.json()
        this.setState({arrivalList: data})
    }

    handleArrivalChange = (event) => {
        this.setState({arrivalPointId: event})
    }

    closeDrawer = () => {
        this.drawer._root.close()
    }

    openDrawer = () => {
        this.drawer._root.open()
    }

    render() {
        return (
            <Drawer
                ref={(ref) => {
                    this.drawer = ref
                }}
                content={<Sidebar navigation={this.props.navigation}/>}
                onClose={() => this.closeDrawer()}>
                <Container style={{backgroundColor: '#F6F7FF'}}>
                    <Content>
                        <LinearGradient style={styles.HeaderContainer} colors={['#fda90a', '#fe4f78']}>
                            <View style={styles.Header}>
                                <TouchableOpacity onPress={this.openDrawer} style={styles.HeaderMargin}>
                                    <Hamburger width={20} height={14}/>
                                </TouchableOpacity>
                                <View style={styles.HeaderMargin}>
                                    <Text style={styles.HeaderText}>Hello, {this.state.user.firstName}</Text>
                                </View>
                            </View>
                        </LinearGradient>
                        <View style={styles.ChooseRouteContainer}>
                            <View style={styles.ChooseRoute}>
                                <Text style={styles.FromDestinationText}>FROM</Text>
                                <Picker
                                    textStyle={{fontFamily: 'Montserrat-Medium'}}
                                    mode="dropdown"
                                    iosIcon={<Icon name="arrow-down"/>}
                                    style={styles.RoutePicker}
                                    selectedValue={this.state.departurePoint}
                                    onValueChange={this.handleDepartureChange}

                                >
                                    {this.state.route.map(route => (
                                        <Picker.Item key={route.key} label={route.name} value={route.key}/>
                                    ))}
                                </Picker>
                                <Text style={styles.FromDestinationText}>DESTINATION</Text>
                                <Picker
                                    mode="dropdown"
                                    iosIcon={<Icon name="arrow-down"/>}
                                    style={styles.RoutePicker}
                                    selectedValue={this.state.arrivalPointId}
                                    onValueChange={this.handleArrivalChange}
                                >
                                    {this.state.arrivalList.map(route => (
                                        <Picker.Item key={route.id}
                                                     label={routeMapping(route.arrival)}
                                                     value={route.id}/>
                                    ))}
                                </Picker>
                            </View>
                        </View>
                        <View>
                            <Text style={styles.DepartureDateText}>DEPARTURE DATE</Text>
                        </View>
                        <View style={styles.chooseDateContainer}>
                            <View style={styles.chooseDate}>
                                <Picker
                                    mode="dropdown"
                                    iosIcon={<Icon name="arrow-down"/>}
                                    onValueChange={this.changeDepartureDate}
                                    selectedValue={this.state.departureDate}
                                    style={styles.RoutePicker}
                                >
                                    {dayList.map((day) => (
                                        <Picker.Item key={day.key} label={day.name} value={day.key}/>
                                    ))}
                                </Picker>
                            </View>
                        </View>
                        <TouchableOpacity style={styles.FindYourBusContainer} onPress={this.handleFindTimetable}>
                            <LinearGradient start={{x: 0, y: 1}}
                                            end={{x: 1, y: 1}}
                                            style={styles.FindYourBus}
                                            colors={['#fda90a', '#fe4f78']}>
                                <Text style={styles.FindYourBusText}>FIND YOUR BUS</Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    </Content>
                </Container>
            </Drawer>
        )
    }
}

const styles = StyleSheet.create({
    chooseDateContainer: {
        width: '100%',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: 360,
    },
    chooseDate: {
        backgroundColor: 'white',
        borderRadius: 10,
        width: 320,
        fontSize: 12,
        fontFamily: 'Montserrat-Medium',
        color: '#2A2D33',
    },
    HeaderContainer: {
        display: 'flex',
        textAlign: 'center',
        justifyContent: 'center',
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
    },
    Header: {
        width: '100%',
        height: 160,
    },
    HeaderText: {
        fontSize: 24,
        fontFamily: 'Montserrat-ExtraBold',
        color: 'white',
    },
    titleContainer: {
        width: '100%',
        height: 100,
    },
    HeaderMargin: {
        marginLeft: 16,
        marginTop: 16
    },
    ChooseRouteContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        width: '100%',
        height: 150,
    },
    ChooseRoute: {
        borderRadius: 15,
        backgroundColor: 'white',
        width: 320,
        height: 200,
        position: 'absolute',
        top: -48,
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 10,
    },
    FromDestinationText: {
        fontSize: 14,
        fontFamily: 'Montserrat-ExtraBold',
        color: '#2A2D33',
        marginLeft: 24,
        marginTop: 16
    },
    RoutePicker: {
        width: undefined,
        fontSize: 12,
        // fontFamily: 'Montserrat-Medium',
        color: '#2A2D33',
        marginLeft: 16
    },
    DepartureDateContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        width: '100%',
        height: 100,
    },
    DepartureDate: {
        flex: 1,
        flexDirection: 'row',
        textAlign: 'center',
        borderRadius: 15,
        backgroundColor: 'white',
        height: 48,
        position: 'absolute',
        top: 8,
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 10,
    },
    DepartureDateText: {
        fontSize: 14,
        fontFamily: 'Montserrat-ExtraBold',
        color: '#2A2D33',
        marginLeft: 48,
        marginTop: 20
    },
    FindYourBusContainer: {
        marginTop: 100,
        flexDirection: 'column',
        justifyContent: 'flex-end',
        alignItems: 'center',
        width: '100%',
        height: 100,
    },
    FindYourBus: {
        flex: 1,
        borderRadius: 20,
        width: 300,
        height: 48,
        position: 'absolute',
        top: 8,
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    FindYourBusText: {
        fontSize: 14,
        fontFamily: 'Montserrat-ExtraBold',
        color: 'white'
    },
    pickerItem: {
        fontFamily: 'Montserrat-ExtraBold',
    }
})

export default Route