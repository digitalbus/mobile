import React from 'react'
import { View, Container, Content } from 'native-base'
import { Text, AsyncStorage, Button } from 'react-native'
import Queuecard from '../components/QueueCard'
import QRCode from 'react-native-qrcode'
import moment from 'moment'
import env from 'react-native-config'


export default class extends React.Component {
    static navigationOptions = {
        title: 'Queue',
    }

    constructor(props) {
        super(props)
        this.state = {
            queue: {
                id: undefined
            },
            timetable: undefined,
            counter: 0,
            timer: null,
            timeLeft: undefined

        }
    }

    componentWillUnmount = () => {
        clearInterval(this.state.timer)
        // console.log('proops',this.props.navigation.navigate('Reserve',{"queue":queue}) )
        console.log('proops',this.props.navigation)
    }

    countDownTime = () => {
        let now = moment()
        let { timetable } = this.state
        if (timetable) {
            //RealTime
            let currentTime = moment()
            //Mockup Time
            // let currentTime = moment(`2019-02-08 7:28:00`)
            let targetTime = moment(`${moment().format('YYYY-MM-DD')} ${timetable.time}`)
            let differnce = targetTime.diff(currentTime)
            // let timeLeft = moment.duration(differnce)
            this.setState({ timeLeft: differnce })
            console.log('currentTime',currentTime)
            console.log('targetTime',targetTime)
            console.log('Dif',differnce)
        }
    }

    componentDidMount = async () => {
        console.log('Moment')
        let queueData = this.props.navigation.getParam('queue', 0)
        if (queueData) {
            let timetable = await fetch(`${env.system_service}/system/timetable/${queueData.timetableId}`)
            timetable = await timetable.json()
            this.setState({ queue: queueData, timetable })
            let localStorage = await AsyncStorage.setItem('queue', JSON.stringify(queueData));
            console.log('timetable', timetable)
            let timer = setInterval(this.thick, 1000)
            this.setState({ timer })
        }
    }

    thick = () => {
        this.countDownTime()
    }

    deleteQueue = async () => {
        await AsyncStorage.removeItem('queue')
    }

    render = () => {
        let { timetable } = this.state
        return (
            <Container>
                <Content>
                    {timetable ? (
                        <Queuecard route={this.state.timetable}
                            queueId={this.state.queue.id} >
                            {this.state.timeLeft ? (
                                <View>
                                    <Text>{moment.duration(this.state.timeLeft).hours()} Hours</Text>
                                    <Text>{moment.duration(this.state.timeLeft).minutes()} Minutes</Text>
                                </View>
                            ) : null}
                        </Queuecard>
                    ) : null}
                    <Button onPress={this.deleteQueue} title={'delete'} />
                </Content>
            </Container>)
    }
}