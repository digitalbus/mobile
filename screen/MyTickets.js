import React from 'react'
import {AsyncStorage, StyleSheet, Text, TouchableOpacity, View} from 'react-native'
import {Button, Container, Content, Drawer} from 'native-base'
import LinearGradient from 'react-native-linear-gradient'
import env from 'react-native-config'
import Sidebar from "../components/Sidebar"
import Hamburger from "../assets/img/hamburgerbar.svg"
import {routeMapping} from "../utils/MappingUtils"
import moment from "moment"
import FromAndDestination from "../components/FromAndDestination";
import {Col, Grid} from "react-native-easy-grid";

class MyTickets extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            tickets: []
        }
    }

    closeDrawer = () => {
        this.drawer._root.close()
    }

    openDrawer = () => {
        this.drawer._root.open()
    }

    componentDidMount = async () => {
        let targetPassenger = await JSON.parse(await AsyncStorage.getItem('passenger'))
        let response = await fetch(`${env.system_service}/queue/myticket/${targetPassenger.id}`)
        response = await response.json()
        this.setState({tickets: response})
        await console.log(response == true)
    }

    pressTicket = (ticketId) => {
        this.props.navigation.navigate('Ticket', {'queue': {'id': ticketId}})
    }

    getAQueue = () => {
        this.props.navigation.navigate('Route')
    }


    render() {
        return (
            <Drawer
                ref={(ref) => {
                    this.drawer = ref
                }}
                content={<Sidebar navigation={this.props.navigation}/>}
                onClose={() => this.closeDrawer()}>
                <Container style={{backgroundColor: '#F6F7FF'}}>
                    <Content>
                        <LinearGradient style={styles.HeaderContainer} colors={['#fda90a', '#fe4f78']}>
                            <View style={styles.Header}>
                                <TouchableOpacity onPress={this.openDrawer} style={styles.HeaderMargin}>
                                    <Hamburger width={20} height={14}/>
                                </TouchableOpacity>
                                <View style={styles.HeaderMargin}>
                                    <Text style={styles.HeaderText}>My Tickets</Text>
                                </View>
                            </View>
                        </LinearGradient>

                        {
                            this.state.tickets.length > 0 ? (
                                this.state.tickets.map((ticket, index) => (
                                    <TouchableOpacity style={styles.ticketList} key={ticket.id}
                                                      onPress={() => this.pressTicket(ticket.id)}>
                                        <View>
                                            <Text style={index == 0 ? styles.firstUpperRip : styles.upperRip}/>
                                        </View>

                                        <View style={{
                                            borderRadius: 15,
                                            backgroundColor: 'white',
                                            width: 80,
                                            height: 160,
                                            flex: 1,
                                            position: 'absolute',
                                            top: -100,
                                            left: 42,
                                            textShadowColor: 'rgba(0, 0, 0, 0.75)',
                                            textShadowOffset: {width: -1, height: 1},
                                            textShadowRadius: 10,
                                        }}>

                                        </View>

                                        <View style={styles.ticket}>
                                            <View style={styles.ticketText}>
                                                {/*<Text style={styles.time}>*/}
                                                {/*    {routeMapping(ticket.timetable.route.departure)} -> {routeMapping(ticket.timetable.route.arrival)}*/}
                                                {/*</Text>*/}

                                                <FromAndDestination queue={{timetable: ticket.timetable}}
                                                                    innerStyleFrom={styles.routeFrom}
                                                                    arrow={styles.rightArrowStyle}
                                                                    innerStyleDestination={styles.routeDestination}/>
                                            </View>

                                            <Grid>
                                                <Col style={{height: 40, marginLeft: 48, marginTop: 12}}>
                                                    <View>
                                                        <Text style={styles.ticketInfoHeader}>Date</Text>
                                                        <Text style={styles.ticketInfo}>{ticket.timetable.day}</Text>
                                                    </View>

                                                    <View style={{marginTop: 16}}>
                                                        <Text style={styles.ticketInfoHeader}>Queue No.</Text>
                                                        <Text style={styles.ticketInfo}>{ticket.queueNumber}</Text>
                                                    </View>
                                                </Col>
                                                <Col style={{height: 40, marginRight: -20, marginTop: 12}}>
                                                    <View>
                                                        <Text style={styles.ticketInfoHeader}>Time</Text>
                                                        <Text
                                                            style={styles.ticketInfo}>{moment(ticket.timetable.time, 'h:mm:ss').format('h:mm a')}</Text>
                                                    </View>

                                                    <View style={{marginTop: 16}}>
                                                        <Text style={styles.ticketInfoHeader}>Type</Text>
                                                        <Text
                                                            style={styles.ticketInfo}>{ticket.timetable.busType}</Text>
                                                    </View>
                                                </Col>
                                            </Grid>
                                        </View>

                                        <View>
                                            <Text style={styles.lowerRip}/>
                                        </View>
                                    </TouchableOpacity>
                                ))
                            ) : (
                                <View
                                    style={{
                                        justifyContent: 'center',
                                        flexDirection: 'column',
                                        alignItems: 'center',
                                        height: 200
                                    }}>
                                    <View>
                                        <Text>Oops! Looks like you don't have any queue</Text>
                                    </View>
                                    <View style={{marginTop: 15}}>
                                        <Button style={{width: 80, justifyContent: 'center'}}
                                                light onPress={this.getAQueue}>
                                            <Text>Get One!</Text>
                                        </Button>
                                    </View>
                                </View>
                            )
                        }
                    </Content>
                </Container>
            </Drawer>
        )
    }
}

let styles = StyleSheet.create({
    HeaderContainer: {
        // display: 'flex',
        // justifyContent: 'center',
        // alignItems: 'center',
        textAlign: 'center',
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        flex: 1,
        flexDirection: 'row',
    },
    Header: {
        width: '100%',
        height: 160,
        flex: 1,
        // position: 'absolute',
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 10,
    },
    HeaderTextSmallFont: {
        fontSize: 14,
        fontFamily: 'Montserrat-SemiBold',
        color: 'white',
    },
    HeaderText: {
        fontSize: 24,
        fontFamily: 'Montserrat-ExtraBold',
        color: 'white',
        textAlign: 'center',
    },
    titleContainer: {
        width: '100%',
        height: 100,
    },
    HeaderMargin: {
        marginLeft: 16,
        marginTop: 16
    },
    HeaderMarginText: {
        marginLeft: 16,
        marginTop: 48,
        position: 'absolute',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    TimetableContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        width: '100%',
        height: 100,
    },
    ticket: {
        flex: 1,
        justifyContent: 'space-between',
        borderRadius: 15,
        backgroundColor: 'white',
        width: 260,
        height: 160,
        position: 'absolute',
        top: -100,
        right: 32,
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 10,
    },
    ticketText: {
        fontSize: 12,
        flexDirection: 'row',
        justifyContent: 'space-between',
        fontFamily: 'Montserrat-ExtraBold',
        color: '#2A2D33',
        marginLeft: 24,
        marginTop: 8
        // marginBottom: 6
    },
    icon: {
        color: 'white'
    },
    routeFrom: {
        fontSize: 12,
        color: '#2A2D33',
        fontFamily: 'Montserrat-ExtraBold',
        marginLeft: 23,
        // marginRight: 8,
        marginTop: 16,
    },
    routeDestination: {
        fontSize: 12,
        color: '#2A2D33',
        fontFamily: 'Montserrat-ExtraBold',
        marginLeft: 1,
        marginTop: 16,
    },
    rightArrowStyle: {
        marginTop: 13,
        marginLeft: -6
    },
    seatsLeft: {
        fontSize: 24,
        color: '#FE4F78',
        marginRight: 36,
        marginTop: 20
    },
    seatsLeftText: {
        marginRight: 20,
    },
    ticketList: {
        flex: 1,
        marginTop: 60,
        flexDirection: 'row',
        justifyContent: 'center',
        width: '100%',
        height: 125,
    },
    ticketInfo: {
        fontFamily: 'Montserrat-Medium',
        color: '#2A2D33',
        justifyContent: 'space-between',
        fontSize: 12,
    },
    ticketInfoHeader: {
        fontFamily: 'Montserrat-Bold',
        color: '#2A2D33',
        justifyContent: 'space-between',
        fontSize: 12,
    },
    upperRip: {
        width: 25,
        height: 25,
        top: -113,
        left: -97,
        position: 'absolute',
        backgroundColor: '#F6F7FF',
        borderRadius: 25,
        zIndex: 1
    },
    firstUpperRip: {
        width: 25,
        height: 25,
        top: -113,
        left: -97,
        position: 'absolute',
        backgroundColor: '#fe675b',
        borderRadius: 25,
        zIndex: 1
    },
    lowerRip: {
        width: 25,
        height: 25,
        top: 48,
        left: -97,
        position: 'absolute',
        backgroundColor: '#F6F7FF',
        borderRadius: 25,
        zIndex: 1
    }
})

export default MyTickets