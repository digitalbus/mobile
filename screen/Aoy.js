import React from 'react'
import {AsyncStorage, Dimensions, ImageBackground, StyleSheet, Text, TouchableOpacity, View} from "react-native"
import {Button, Container, Content, Form, Input, Item, Label} from "native-base"
import SplashScreen from 'react-native-splash-screen'
import LoginBG from '../assets/img/LoginBG.jpg'
import env from 'react-native-config'

const {width, height} = Dimensions.get('window')
type Props = {};

class Aoy extends React.Component<Props> {
    constructor(props) {
        super(props)
        this.state = {
            username: undefined,
            password: undefined
        }
    }

    componentDidMount = async () => {
        SplashScreen.hide()
        let passenger = await JSON.parse(await AsyncStorage.getItem("passenger"))
        if (passenger) {
            this.props.navigation.navigate('Route')
        }

    }

    usernameChange = (event) => {
        let username = event
        this.setState({username})
    }

    passwordChange = (event) => {
        let password = event
        this.setState({password})

    }

    handleUserLogin = async () => {
        console.log('press login')
        let response = await fetch(`${env.system_service}/system/user/login`, {
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
                "username": this.state.username,
                "password": this.state.password
            }),
            method: "POST"
        })
        response = await response.json()
        if (response.firstName) {
            await AsyncStorage.setItem('passenger', JSON.stringify(response))
            await this.props.navigation.navigate('Route')
        } else {
            console.log('response', response)
        }
    }

    render() {
        return (
            <Container>
                <Content>
                    <View style={styles.appText}>
                        <ImageBackground source={LoginBG} style={styles.background} blurRadius={2}>
                            <View style={styles.titleContainer}>
                                <Text style={styles.appTitle}>B O U R N EY</Text>
                            </View>
                            <Form style={{marginTop: 33}}>
                                <Item fixedLabel>
                                    <Label style={[styles.appText]}>Username</Label>
                                    <Input value={this.state.username} onChangeText={this.usernameChange}
                                           style={[styles.appText]}/>
                                </Item>
                                <Item fixedLabel>
                                    <Label style={styles.appText}>Password</Label>
                                    <Input secureTextEntry type={'password'} value={this.state.password}
                                           onChangeText={this.passwordChange} style={styles.appText}/>
                                </Item>
                                <TouchableOpacity
                                    style={{justifyContent: 'center', flexDirection: 'row', marginTop: 20}}
                                    onPress={this.handleUserLogin}>
                                    <Button style={{textAlign: 'center', width: 100}}
                                            onPress={this.handleUserLogin}>
                                        <Text style={styles.loginButton}>Login</Text>
                                    </Button>
                                </TouchableOpacity>
                            </Form>
                        </ImageBackground>
                    </View>
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    appTitle: {
        fontSize: 40,
        fontFamily: 'Montserrat-ExtraBold',
        color: 'white',
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 10
    },
    appText: {
        fontSize: 19,
        color: '#fff',
        fontFamily: 'Montserrat-Regular',

    },
    background: {
        width: width,
        height: height
    },
    titleContainer: {
        width: '100%',
        height: 100,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 180
    },
    buttonStyle: {
        width: '100%',
        height: 100,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonCenter: {
        textAlign: 'center'
    },
    loginButton: {
        color: 'white',
        alignItems: 'center',
        marginLeft: 12,
        marginRight: 12
    }
})

export default Aoy