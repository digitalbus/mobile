import React, { Component } from 'react';
import { Container, Header, Content, Form, Item, Input, Left, Right, Button, Icon, Body, Title, Text } from 'native-base';

export default class Login extends Component {
  render() {
    return (
      <Container>
         <Header>
          <Left/>
          <Body>
            <Title>Login</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <Form>
            <Item>
              <Input placeholder="Username" />
            </Item>
            <Item last>
              <Input placeholder="Password" />
            </Item>
          </Form>
          <Button rounded>
            <Text>Login</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}