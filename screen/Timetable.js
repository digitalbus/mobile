import React from 'react'
import {ActivityIndicator, AsyncStorage, StyleSheet, Text, View} from 'react-native'
import {Button, Container, Content} from 'native-base'
import LinearGradient from 'react-native-linear-gradient'
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome'
import {faAngleLeft} from '@fortawesome/free-solid-svg-icons'
import env from 'react-native-config'
import {routeMapping} from '../utils/MappingUtils'
import moment from "moment"
import {Dialog, DialogButton, DialogContent, DialogFooter, SlideAnimation} from "react-native-popup-dialog"
import {HTTP} from "../utils/StatusCodes"
import SockJsClient from 'react-stomp'
import Geolocation from "@react-native-community/geolocation"
import TimetableCard from "../components/TimetableCard"
import RequestCard from "../components/RequestCard"
import FromAndDestination from "../components/FromAndDestination"


const timeFormat = time => moment(time, 'hh:mm:ss').format('h:mm a')

class Timetable extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            targetRouteId: undefined,
            departureDate: undefined,
            route: {
                departure: undefined,
                arrival: undefined
            },
            timetables: [],
            dialogVisibility: false,
            selectedTimetable: {
                time: ''
            },
            warningMessage: undefined,
            loading: true
        }
    }

    componentDidMount = async () => {
        await this.fetch()
        Geolocation.getCurrentPosition(location => {
            this.setState({geoLocation: location})
        })

    }

    selectTimetable = (timetable) => {
        this.setState({dialogVisibility: true, selectedTimetable: timetable})
    }


    goBack = () => {
        this.props.navigation.goBack()
    }

    requestQueue = ()=>{
        let departureDate = this.props.navigation.getParam('departureDate', 'monday')
        let targetRouteId = this.props.navigation.getParam('routeId', 1)
        let {selectedTimetable} = this.state
        this.props.navigation.navigate('ReservedTimetable',{'departureDate': departureDate, 'routeId':targetRouteId,'selectedTimetableId': selectedTimetable.id})
    }

    bookQueue = async () => {
        let {selectedTimetable} = await this.state
        let passenger = await JSON.parse(await AsyncStorage.getItem('passenger'))
        let data
        try {
            data = await fetch(`${env.system_service}/queue`,
                {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        'passenger': {'id': passenger.id},
                        'timetable': {'id': selectedTimetable.id},
                        'queueType': "booked",
                        'latitude': this.state.geoLocation.coords.latitude,
                        'longitude': this.state.geoLocation.coords.longitude,
                    }),
                })
            if (data.status == HTTP.OK) {
                data = await data.json()
                await this.setState({dialogVisibility: false})
                console.log('reserve', data)
                await this.props.navigation.navigate('Ticket', {"queue": data})
            } else if (data.status == HTTP.CONFLICT) {
                this.setMessage('You already reserved this round !')
            } else if (data.status == HTTP.HTTP_LOCKED) {
                this.setMessage('Queue is full ! try another round')
            } else {
                this.setMessage('Oos! Something went wrong!')
            }
        } catch (error) {
            console.error(error)
        }
        console.log(data)
    }

    setMessage = (message) => {
        this.setState({warningMessage: message, dialogVisibility: false})

    }

    fetch = async () => {
        let departureDate = this.props.navigation.getParam('departureDate', 'monday')
        let targetRouteId = this.props.navigation.getParam('routeId', 1)
        this.setState({targetRouteId})
        let response = await fetch(`${env.system_service}/system/route/${targetRouteId}`)
        response = await response.json()
        let timetables = await fetch(`${env.system_service}/system/timetable/routebytime/${targetRouteId}?day=${departureDate}`)
        timetables = await timetables.json()
        this.setState({route: response, timetables, loading: false})
    }


    dialogWhenSeatIsNotAvailable = () => {
        return (
            <Dialog
                modalAnimation={new SlideAnimation({
                    slideFrom: 'bottom',
                })}
                visible={this.state.dialogVisibility}
                footer={
                    <DialogFooter>
                        <DialogButton
                            text="Cancel"
                            textStyle={styles.dialogButtonText}
                            onPress={() => {
                                this.setState({dialogVisibility: false})
                            }}
                        />
                        <DialogButton
                            text="Yes"
                            textStyle={styles.dialogButtonText}
                            onPress={this.requestQueue}
                        />
                    </DialogFooter>
                }
            >
                <DialogContent>
                    {/*<Text style={{marginTop: 10, fontSize: 18}}>*/}
                    <Text style={styles.dialogContentText}>🚫 This round is fully booked! 🚫</Text>
                    <Text style={styles.dialogContentText}>Do you want to request the new bus? 🚌✨</Text>
                    {/*</Text>*/}
                </DialogContent>
            </Dialog>
        )
    }

    dialogWhenSeatIsAvailable = () => {
        return (
            <Dialog
                modalAnimation={new SlideAnimation({
                    slideFrom: 'bottom',
                })}
                visible={this.state.dialogVisibility}
                footer={
                    <DialogFooter>
                        <DialogButton
                            text="Cancel"
                            textStyle={styles.dialogButtonText}
                            onPress={() => {
                                this.setState({dialogVisibility: false})
                            }}
                        />
                        <DialogButton
                            text="Yes"
                            textStyle={styles.dialogButtonText}
                            onPress={this.bookQueue}
                        />
                    </DialogFooter>
                }
            >
                <DialogContent>
                    <Text style={styles.dialogContentText}>Are you sure
                        to book on {timeFormat(this.state.selectedTimetable.time)} ?
                    </Text>
                </DialogContent>
            </Dialog>
        )
    }

    render() {
        return (
            <Container style={{backgroundColor: '#F6F7FF'}}>
                {this.state.dialogVisibility && this.state.selectedTimetable.availableSeat > 0 ? this.dialogWhenSeatIsAvailable() : this.dialogWhenSeatIsNotAvailable()}
                {this.state.warningMessage ? (
                    <Dialog visible>
                        <DialogContent>
                            <Text style={{marginTop: 10, fontSize: 18}}>{this.state.warningMessage}</Text>
                        </DialogContent>
                        <DialogFooter>
                            <DialogButton
                                textStyle={{fontSize: 15}}
                                text="OK"
                                onPress={() => this.setState({warningMessage: undefined})}
                            />
                            <Text/>
                        </DialogFooter>
                    </Dialog>
                ) : null}
                <Content>
                    <LinearGradient style={styles.HeaderContainer} colors={['#fda90a', '#fe4f78']}>
                        <View style={styles.Header}>
                            <View style={styles.HeaderMargin}>
                                <FontAwesomeIcon onPress={this.goBack} icon={faAngleLeft}
                                                 style={styles.icon} size={24}/>
                            </View>
                            <View style={styles.HeaderMarginText}>
                                <Text style={styles.HeaderTextSmallFont}>Please select your bus</Text>

                                <FromAndDestination queue={{timetable:{route:this.state.route}}}
                                                    innerStyleFrom={styles.routeFrom}
                                                    arrow={styles.rightArrowStyle}
                                                    white
                                                    innerStyleDestination={styles.routeDestination}/>

                                {/*<Text style={styles.HeaderTextSmallFont}>Thursday, August 22nd, 2019</Text>*/}
                            </View>
                        </View>
                    </LinearGradient>
                    {!this.state.loading ? (
                        this.state.timetables.length > 0 ? (
                            this.state.timetables.map(timetable => (
                                <TimetableCard selectTimetable={() => this.selectTimetable(timetable)}
                                               key={timetable.id} timetable={timetable}/>
                            ))
                        ) : (
                            <View
                                style={{
                                    justifyContent: 'center',
                                    flexDirection: 'column',
                                    alignItems: 'center',
                                    height: 200
                                }}>
                                <View>
                                    <Text>No Timetable Available</Text>
                                </View>
                                <View style={{marginTop: 6}}>
                                    <Button style={{width: 80, justifyContent: 'center'}} light
                                            onPress={this.fetch}><Text>Reload</Text>
                                    </Button>
                                </View>
                            </View>
                        )
                    ) : (
                        <View style={[styles.container, styles.horizontal]}>
                            <ActivityIndicator size="large" color="#fea90a"/>
                        </View>
                    )}
                </Content>
                <SockJsClient url={`${env.system_service}/gs-guide-websocket`}
                              topics={[`/queue/emptyseat/${this.state.targetRouteId}`]}
                              onConnect={console.log("Connection established!")}
                              onDisconnect={console.log("Disconnected!")}
                              debug={true}
                              onMessage={(msg, topic) => this.fetch()}
                              ref={(client) => {
                                  this.clientRef = client
                              }}
                />
            </Container>
        )
    }
}

let styles = StyleSheet.create({
    HeaderContainer: {
        // display: 'flex',
        // justifyContent: 'center',
        // alignItems: 'center',
        textAlign: 'center',
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        flex: 1,
        flexDirection: 'row',
    },
    Header: {
        width: '100%',
        height: 250,
        flex: 1,
        // position: 'absolute',
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 10,
    },
    HeaderTextSmallFont: {
        fontSize: 14,
        fontFamily: 'Montserrat-SemiBold',
        color: 'white',
    },
    HeaderText: {
        fontSize: 20,
        fontFamily: 'Montserrat-ExtraBold',
        color: 'white',
    },
    HeaderMargin: {
        marginLeft: 16,
        marginTop: 32,
    },
    HeaderMarginText: {
        marginTop: 30,
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column'
    },
    Timetable: {
        flex: 1,
        justifyContent: 'space-between',
        borderRadius: 15,
        backgroundColor: 'white',
        width: 350,
        height: 88,
        position: 'absolute',
        top: -48,
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 10,
    },
    TimetableText: {
        fontSize: 12,
        flexDirection: 'row',
        justifyContent: 'space-between',
        fontFamily: 'Montserrat-ExtraBold',
        color: '#2A2D33',
        marginLeft: 24
    },
    icon: {
        color: 'white'
    },
    time: {
        fontFamily: 'Montserrat-ExtraBold',
        fontSize: 24,
        color: '#2A2D33',
        marginTop: 20
    },
    seatsLeft: {
        fontSize: 24,
        fontFamily: 'Montserrat-ExtraBold',
        color: '#FE4F78',
        marginRight: 45,
        marginTop: 20
    },
    spinnerContainer: {
        flex: 1,
        justifyContent: 'center'
    },
    horizontal: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        padding: 10
    },
    dialogContentText: {
        fontSize: 14,
        fontFamily: 'Montserrat-SemiBold',
        color: '#2A2D33',
        marginTop: 10,
        textAlign: 'center'
    },
    dialogButtonText: {
        fontSize: 14,
        fontFamily: 'Montserrat-SemiBold',
        color: '#4A7FEA',
        marginTop: 10,
        textAlign: 'center'
    },
    routeFrom: {
        fontSize: 16,
        color: 'white',
        fontFamily: 'Montserrat-ExtraBold',
    },
    routeDestination:{
        fontSize: 16,
        color: 'white',
        fontFamily: 'Montserrat-ExtraBold',
    },
    rightArrowStyle: {
        marginTop: 2
    }
})

export default Timetable