/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image,AsyncStorage } from 'react-native'
import { createStackNavigator, createAppContainer } from 'react-navigation'
import { Button, Container, Content, Form, Item, Input, Label } from 'native-base'
import env from 'react-native-config'

type Props = {};
export default class App extends Component<Props> {

  state = {
    
  }
  
  handleChangeScreen = async () => {
    // let {username,password} = this.state
    //     if(username && password) {
    //         try{
    //             let data = await fetch('http://localhost:8084/userservice/auth',{
    //                 method: 'POST',
    //                 headers: { 'Content-Type': 'application/json' },
    //                 body:JSON.stringify({
    //                     username,
    //                     password
    //                 })
    //             })
    //             data = await data.json()
    //             console.log('data',data)
    //             if(data.id){
    //               await AsyncStorage.setItem('user',JSON.stringify(data))
    //               this.props.navigation.navigate('Route')
    //             }
    //             console.log(data)
    //         }catch(error){
    //             console.log(error)
    //         }
    //     }
    this.props.navigation.navigate('Route')
  }

  componentDidMount = () => {
    console.log(`foo is equal to ${env.system_service}`)
  }
  
  handleInputChange = (value,name) => {
    let {state} = this
    this.setState({...state,[name]:value})
  }

  render() {
    return (
      <Container>
        <Content>
          <View style={{
            justifyContent: 'center',
            flexDirection: 'row',
          }}>
            <Image source={require('../assets/img/bn-logo-1.png')} style={{ width: 250, height: 250 }} />
          </View>
          <View style={{
            justifyContent: 'center',
            flexDirection: 'row',
            size:'48px'
          }}>
            <Text>Bourney</Text>
          </View>
          <Item fixedLabel>
            <Label>Username</Label>
            <Input type={'password'} onChangeText={(text) => {this.handleInputChange(text,'username')}} />
          </Item>
          <Item fixedLabel last>
            <Label>Password</Label>
            <Input onChangeText={(text) => {this.handleInputChange(text,'password')}}  name={'password'} />
          </Item>
          <View style={{
            paddingTop:40,
            justifyContent: 'center',
            flexDirection: 'row',
          }}>
            <Button onPress={this.handleChangeScreen} rounded  style={{width:80}}><Text> Login! </Text></Button>
          </View>
        </Content>
      </Container >
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
