import React from "react"
import {createAppContainer, createStackNavigator} from "react-navigation"
import HomeScreen from './screen/Home'
import ProfileScreen from './screen/Profile'
import LoginScreen from './screen/Login'
import Aoy from "./screen/Aoy"
import Route from "./screen/Route"
import Timetable from "./screen/Timetable"
import ReservedTimetable from './screen/ReservedTimetable'
import TicketScreen from "./screen/TicketScreen"
import MyTickets from "./screen/MyTickets"
import RequestedScreen from './screen/RequestedScreen'
import Test from "./screen/Test"
import './utils/notification'


const AppNavigator = createStackNavigator({
        Aoy: Aoy,
        Route: Route,
        MyTickets: MyTickets,
        Timetable: Timetable,
        ReservedTimetable: ReservedTimetable,
        Home: HomeScreen,
        Login: LoginScreen,
        Profile: ProfileScreen,
        Ticket: TicketScreen,
        RequestedScreen:RequestedScreen,
        Test: Test
    },
    {
        headerMode: 'none'
    })

export default createAppContainer(AppNavigator)
