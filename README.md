# mobile

### Android file installation
please install these in android folder
https://github.com/crazycodeboy/react-native-splash-screen
https://reactnavigation.org/docs/en/getting-started.html
https://github.com/react-native-community/react-native-linear-gradient

#### (Splash Screen issue)
 cannot find symbol     
 SplashScreen.show(this); 
https://github.com/crazycodeboy/react-native-splash-screen/issues/217

### RNPushNotification issue
it will have android manifest debug fail
```
First: go to the 'node_modules\react-native-push-notification\android\build.gradle' and add these two lines:

def DEFAULT_GOOGLE_PLAY_SERVICES_VERSION = "+"
def DEFAULT_FIREBASE_MESSAGING_VERSION = "+"
then go to 'Your_project_folder\android\app\build.gradle' and replace
implementation project('react-native-push-notification')
with

compile (project(':react-native-push-notification')) {
        exclude group: "com.google.android.gms", module: "play-services-gcm"
        exclude group: "com.google.firebase", module: "firebase-messaging"
 }
```
ref: https://github.com/zo0r/react-native-push-notification/issues/1149

