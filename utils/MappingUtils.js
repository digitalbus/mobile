export const routeMapping =(routeKey) => {
    switch (routeKey) {
        case "BMD":
            return "Bangmod"
        case "BKT":
            return "Bangkhuntien"
        case "KNX":
            return "KX Building"
        case "TLT":
            return "Lotus Rama 2"
        default:
            return "Loading.."
    }
}