export const HTTP ={
    OK: 200,
    BAD_REQUEST: 400,
    CONFLICT: 409,
    HTTP_LOCKED: 423
}